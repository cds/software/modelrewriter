# Scripts for rewriting or otherwise parsing simulink models.

# Setting up and updating models.

1. On boot server, clone this project and cd into it.
2. create virtual environment with ```python3 -m venv venv```
3. ```source venv/bin/activate```
4. ```python3 -m pip install ply```
5. ```./create_mdl_list.py -a``` to create /opt/rtcds/usrapps/mdlfiles.txt
6. On a system with matlab (2017b or newer) that can access /opt/rtcds/userapps/mdlfiles.txt, clone this project and cd into it.
7. (optional) edit 'convert_matlab'. add ```-P n``` as argument to xargs where n is the number of cores you want to use. 
8. ```./convert_matlab```.  This will take a while.    Will create 'to19a.txt' file as output log.  Old files are backed up in <model>.mdl.oldmatlabversion
9. Back on bootserver, run ```./model_checks.py -a```.  This checks that the scripts can read all models.  Make sure there are no failures before proceeding.
10. ```./update4_2_0.py -a``` and check output.  This will update parameters in all models.  Old file is backed up as <model>.mdl.rcg4_0 or similar.

# the scripts

## create_mdl_list.py

find mdl files for every given model, then write them out to /opt/rtcds/userapps/mdlfiles.txt.

## model_checks.py

Parses models, creates a new mdl file in memory from the parse tree, parses that new file to make sure the output parses the same as the input.

## udpate4_2_0.py 

Rewrite the model parameter block, changing some names, deleteing some unused parameters.  ipc_rate set to 2048 for SUS iops.

Output matches desired parameters for version 4.2.0

Will not rewrite models that are already up to date.

## move_ifo.py

Given a two letter ifo string as an argument, convert the mdl file to the new ifo.  Also will look back in parent directories to change directories that are named after the ifo.

Will overwrite existing files of the new ifo!

## svn_check.py

Report for each model whether it's been checked in to SVN.

# About these scripts

These scripts are based on an MDL file parser that uses the 'ply' parsing library.  
Scripts are parsed into a simple tree structure.  
Checks and transformations are then performed.  
An mdl file can be generated from a transformed tree.

The scripts use a main function generator, create_main() in walk_models.py.  The scripts have a single processor function that does the work of the script.
The process function takes a model name and path to mdl file and processes a single file.
The main function created by walk_models.py can than apply that to all the models using '-a' found in /etc/rtsystab, only iop  models with '-i', only user models with '-u'.
```-m <model1> -m <model2> ...``` to run on specific models, or pass direct paths to any number of mdl files.

Associated mdl files are found on RCG_LIB_PATH or there's an error. 

See model_checks.py for a simple example of using create_main().

See create_mdl_list.py for an example of how to add additional args before calling create_main(), then how to process those args after.

The matlab conversion scripts, convert_matlab and loadsave are exceptions.  They are simple bash scripts that just execute a load and save inside matlab.

