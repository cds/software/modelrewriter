#!/bin/env python3
# set parameters to models for running on dts1.
# makes assumptions about which models are dolphin timing models 
import mdl_parser
import shutil
import sys
import os.path as path


def is_cdsParam_block(obj):
    """
    Check if an MDL object is a CDS parameter block
    :param obj: An MDLObject
    :return: True if the object is a CDS paramter block
    """
    if obj.name == "Block" and isinstance(obj.value, mdl_parser.MDLObjectList):
        for x in obj.value:
            if x.name == "Tag" and x.value == "cdsParameters":
                return True
    return False

def is_cdsParam_externalreference(obj):
    """
    Check if an MDL objec is a CDS parameter external reference
    :param obj:
    :return: True if it is sucha reference
    """
    if obj.name == "ExternalFileReference" and isinstance(obj.value, mdl_parser.MDLObjectList):
        for x in obj.value:
            if x.name == 'Reference' and x.value == "cdsParameters/Subsystem":
                return True
    return False


class ParamBlockException(Exception):
    pass


def create_param_string(params, sep):
    """
    Create a param string suitable for an MDL file.
    :param params: A dictionary of the parameters and their values
    :param sep: A separator betwen values.
    :return: A string suitable for an MDL file.
    """

    order = "ifo rate dcuid host iop_model pciRfm dolphin_time_xmit dolphin_time_rcvr specific_cpu rfm_delay".split()
    unused = list(params.keys())

    txts = []
    for p in order:
        if p in params:
            txts.append(f"{p}={params[p]}")
            unused.remove(p)

    for p in unused:
        txts.append(f"{p}={params[p]}")

    return sep.join(txts)


def get_param_block(model, ptree):
    """

    :param model: model name
    :param ptree:  Root object of an mdl file parse tree
    :return:
    """
    param_blocks = ptree.find(is_cdsParam_block)
    if len(param_blocks) == 0:
        raise ParamBlockException(f"Could not find param block in {model}")
    elif len(param_blocks) > 1:
        raise ParamBlockException(f"Found more than 1 param block in {model}")
    return param_blocks[0]


def get_params_from_block(block):
    """
    From a param Block object, get a dictionary of name: value pairs for the parameters
    :param block: an MDLObject representing the param block
    :return: A dictionary of the parameters
    """
    for obj in block.value:
        if obj.name == "Name":
            return {param.split('=', 1)[0] : param.split('=', 1)[1]
                    for param in str(obj.value).split(r'\n')
                    if param.find('=') >= 0}

    raise ParamBlockException("Couldn't find 'Name' object in cdsParameters block.")


def set_params_to_block(block, params):
    for obj in block.value:
        if obj.name == "Name":
            param_txt = create_param_string(params, '\\n')
            obj.value.set_string(param_txt)
            return

    raise ParamBlockException("Couldn't find 'Name' object in cdsParameters block.")


def set_params_to_external_reference(model, ptree, params):
    paramtxt = create_param_string(params, " ")
    txt = f"{model}/{paramtxt}"

    refs = ptree.find(is_cdsParam_externalreference)

    for ref in refs:
        for obj in ref.value:
            if obj.name == 'Path':
                obj.value.set_string(txt)


# upgrades
def change_name(params, old_name, new_name):
    """
    Change a param name from old to new
    :returns: number of items changed
    """
    if old_name in params:
        params[new_name] = params[old_name]
        del params[old_name]
        return 1
    return 0


def delete_param(params, param_name):
    """

    :param params:
    :param param_name:
    :return: number of items changed
    """
    if param_name in params:
        del params[param_name]
        return 1
    return 0


def set_param(params, param_name, param_value):
    if param_name in params and params[param_name] == param_value:
        return 0
    else:
        params[param_name] = param_value;
        return 1

timing_servers = "x2ioplsc0 x2iopiscex x2iopiscey".split()

def upgrade_params(model, params):
    """
    Return a params dictionary changed to match those expected by 4_2_0
    :param params:
    :return: number of items changed
    """    
    global timing_servers
    num_changed = 0
    num_changed += delete_param(params, "dolphin_time_recv")
    if model in timing_servers:
        num_changed += set_param(params, "dolphin_time_xmit","1")
        num_changed += delete_param(params, "dolphin_time_rcvr")
	num_changed += set_param(params, "pciRfm", "1")
    elif model.find("iop") >= 0 && model.find("susaux") < 0:
        num_changed += set_param(params, "dolphin_time_rcvr","1")
        num_changed += delete_param(params, "dolphin_time_xmit")
	num_changed += set_param(params, "pciRfm", "1")
    num_changed += set_param(params, "requireIOcnt","0")
    return num_changed


def upgrade_model(model, mdl_path):
    sys.stdout.write(f'upgrading params in {model}.')
    sys.stdout.flush()
    try:
        ptree = mdl_parser.parse_mdl_file(mdl_path)
    except Exception as e:
        sys.stdout.write(f"..failed parsing: {str(e)}")
    sys.stdout.write('.')
    sys.stdout.flush()
    try:
        block = get_param_block(model, ptree)
    except Exception as e:
        sys.stdout.write(f"..failed getting param block: {str(e)}")
    new_params = get_params_from_block(block)
    sys.stdout.write('.')
    sys.stdout.flush()
    num_changed = upgrade_params(model, new_params)
    sys.stdout.write('.')
    sys.stdout.flush()

    if num_changed > 0:
        set_params_to_block(block, new_params)
        set_params_to_external_reference(model, ptree, new_params)
        # backup
        with open(mdl_path, "wt") as f:
            f.write(str(ptree))
            sys.stdout.write(".done\n")
    else:
        sys.stdout.write(f'model is up to date. File unchanged.\n')


if __name__ == "__main__":
    from walk_models import create_main

    main, args = create_main(description="Update models to set requireIOcnt=0")
    main(upgrade_model)
