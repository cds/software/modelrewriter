# Utilities for finding all MDL files for system models, and for walking
# through the model files to apply procedures
# must be run on bootserver

import os
import os.path as path
import subprocess


def get_model_list():
    """
    :return: list of all model names on bootserver
    """
    return subprocess.getoutput('/etc/rt.sh').split()


def get_iop_list():
    """
        :return: list of all model names on bootserver
        """
    return subprocess.getoutput('/etc/rtiop.sh').split()


def get_rcg_path():
    """
    :return: list of paths the RCG searches for mdl files when building a model.
    """
    rtcds_env = subprocess.getoutput('rtcds env').split('\n')
    for line in rtcds_env:
        if line[:12] == "RCG_LIB_PATH":
            return line.split('=', 1)[1].split(':')
    raise Exception("RCG_LIB_PATH not found in rtcds environment")


class MDLFileNotFoundException(Exception):
    pass


class DuplicateMDLFileException(Exception):
    pass


def find_models(models=None, mdl_paths=None):
    """
    Searches mdl_path list of paths for mdl files named after the model.

    Can throw MDLFileNotFoundException if no mdl file is found for a model.
    Can throw DuplicateMDLFileException if a second mdl is found for a model.

    :param models: list of model names.  If none, use get_model_list()
    :param mdl_paths: list of directory to search for MDL files. If none, use get_rcg_path()
    :return: A hash of { modelname: path to model }, models_not_found, duplicates
    """

    duplicates = []

    if models is None:
        models = get_model_list()

    if mdl_paths is None:
        mdl_paths = get_rcg_path()

    model_record = {}
    for p in mdl_paths:
        if path.exists(p):
            for fname in os.listdir(p):
                name, ext = path.splitext(fname)
                if ext == ".mdl":
                    if name in models:
                        if name in model_record:
                            duplicates.append(name)
                        else:
                            model_record[name] = path.join(p, fname)

    # check for models not found
    not_found = []
    for model in models:
        if model not in model_record:
            not_found.append(model)

    # remove duplicates from record
    for model in duplicates:
        del model_record[model]

    return model_record, not_found, duplicates


def walk_models(proc, models=None, paths=None):
    """
    Call proc for each model in models.  Proc takes the model name and path to mdl file as the first two arguments.

    :param models: a list of model names. If None, use get_model_list()
    :param path: a list of paths to search for mdl files. If None, use get_rcg_path()
    :param proc: a callable that takes a model name and path to mdl file.
    :return: None
    """
    model_record, not_found, duplicates = find_models(models, paths)
    if len(not_found) > 0 or len(duplicates) > 0:
        print("*** Warning: there were some models that could not be processed.\nDetails given after the rest are processed.")
    for model, p in model_record.items():
        proc(model, p)
    if len(not_found) > 0:
        print("*** Warning: an '.mdl' file could not be found for the following models.  They were not processed:")
        for model in not_found:
            print(f"\t{model}")
    if len(duplicates) > 0:
        print("*** Warning: multiple '.mdl' files were found for the following models.  They were not processed:")
        for model in duplicates:
            print(f"\t{model}")


def walk_iop_models(proc, paths=None):
    """
    Call proc for each iop model found in paths.  Proc takes the model name and path to mdl file as the first two options.

    :param proc: a callable that takes a model name and path to mdl file.
    :param paths: A list of paths to search for .mdl files.  If None, use get_rcg_path()
    :return: None
    """
    return walk_models(proc, get_iop_list(), paths)


def walk_user_models(proc, paths=None):
    """
    Call proc for each user model found in paths.  Proc takes the model name and path to mdl file as the first two options.

    :param proc: a callable that takes a model name and path to mdl file.
    :param paths: A list of paths to search for .mdl files.  If None, use get_rcg_path()
    :return: None
    """
    all = get_model_list()
    iop = get_iop_list()
    user = [m for m in all if m not in iop]
    return walk_models(proc, user, paths)


def process_mdl_file(proc, mdl_path):
    """
    Call proc on the mdl file located at mdl_path.

    :param proc: is a callable as used in walk_models that takes a model name and a file path to an mdl file.
    :param mdl_path: Path to an mdl file.  model name is deterimined from path.
    :return:
    """
    model = path.splitext(path.basename(mdl_path))[0]
    proc(model, mdl_path)


def test_callable(model, mdl_file):
    print(f"model={model} mdl_file={mdl_file}")


def create_main(parser=None, description=None):
    """
    Create a main function that can be used in a script to give various options on how to apply the script.

    :param proc:  A process that takes a model name and a path to the mdl file.  This process will be called on one or many models.
    :return: A callable that can be used as a main function.
    """

    import argparse
    import sys

    parser = parser or argparse.ArgumentParser(description=description)
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-a', '--all', help='process all models', action='store_true', required=False)
    group.add_argument('-u', '--user', help='process iop models', action='store_true', required=False)
    group.add_argument('-i', '--iop', help='process all models', action='store_true', required=False)

    parser.add_argument('mdl_path', nargs='*', help='path to mdl file')
    parser.add_argument('-m', '--model', help='find and process model by name',
                        metavar='model_name', action='append')

    args = parser.parse_args()
    if len(args.mdl_path) > 0 and (args.user or args.all or args.iop):
        print("ERROR: 'mdl_path' cannot be specified with -a, -u, or -i options.\n")
        parser.print_usage()
        sys.exit(1)

    def _main(proc, paths=None):
        processed_something = False
        if args.user:
            processed_something = True
            walk_user_models(proc, paths)
        elif args.iop:
            processed_something = True
            walk_iop_models(proc, paths)
        elif args.all:
            processed_something = True
            walk_models(proc, None, paths)
        if len(args.mdl_path) > 0:
            processed_something = True
            for p in args.mdl_path:
                try:
                    process_mdl_file(proc, p)
                except Exception as e:
                    print(f"Failure processing mdl {p}: {str(e)}")
        if args.model is not None and len(args.model) > 0:
            processed_something = True
            walk_models(proc, args.model)
        if not processed_something:
            parser.print_usage()

    return _main, args


def run():
    walk_models(test_callable)


if __name__ == "__main__":
    run()
