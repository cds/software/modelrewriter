#!env python3
# return a parse tree of mdl files as an MDLObjectList instance.

import ply.lex as lex
import ply.yacc as yacc
import sys

class Word(object):
    """
    Represents a bare string as in an enumeration
    """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value

    def __repr__(self):
        return f'Word("{self.value}")'

    def __eq__(self, other):
        return self.value == other.value


def indent_to_ws(indent, start=0):
    """
    Return a string that will advance from starting column to the indent position.

    Returns a single space if start is on or after indent.

    Assumes size 8 tabs.  Use a single tab to get to a column that's multiple of  8,
    then uses tabs until less than 8 columns are left
    columns are to go, then uses spaces again.  This is to match behavior Matlab mdl files.

    :param indent: Number of columns to indent from the left of the screen.
    :param start: Columns to start padding at.
    :return: string that will pad the line out from column start for a total of indent columns from the left of the screen
    """

    #special case of no indent, just return empty string
    if indent == 0:
        return ""

    # if we've already gone past the mark, just skip one column
    if start >= indent:
        indent = start + 1

    to_pad = indent - start

    # get the number of spaces until the next multiple of 8 column
    leading_spaces = (8 - (start % 8)) % 8

    # won't even reach multiple of 8
    if leading_spaces > to_pad:
        return " " * to_pad

    if leading_spaces > 0:
        txt = "\t"
    else:
        txt = ""

    to_pad -= leading_spaces

    txt += "\t" * (to_pad // 8) + " " * (to_pad % 8)

    return txt


class MDLObjectList(list):
    """
    Represents the lists of objects in a compound value
    """

    def indent_print(self, indent):
        txt = "".join([x.indent_print(indent) for x in self])
        return txt

    def __str__(self):
        return self.indent_print(0)

    def find(self, predicate):
        """
        Return any object, or subobject from the list that matches the predicate
        :param predicate: A callable that takes a MDLObject. It returns True of the object matches the predicate
        :return: A list of objects, including recursively any sub objects that match the predicate
        """
        results = [obj.find(predicate) for obj in self]
        matches = []
        for m in results:
            matches += m
        return matches

    def mutate(self, proc):
        """
        Recursively passes proc to all object, potentitially changing them.

        :param proc: Takes an MDLObject and potentially mutates it.
        :return: None
        """
        for obj in self:
            obj.mutate(proc)


class MDLInt(object):
    """Represents an integer."""
    def __init__(self, token):
        """
        :param token:  a string.  the text of the number token
        """
        self.text = token
        self.value = int(token)

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return f'MDLInt("{self}")'

    def __eq__(self, other):
        return self.value == other.value


class MDLFloat(object):
    """Represents a floating point number to be written in decimal format."""

    def __init__(self, token):
        """
        :param token:  a string.  the text of the number token
        """
        self.text = token
        self.value = float(token)
        self.precision = len(token) - token.find('.') - 1

    def __str__(self):
        return f"%.{self.precision}f" % self.value

    def __repr__(self):
        return f'MDLFloat("{self}")'

    def __eq__(self, other):
        return self.value == other.value


class MDLExp(object):
    """Represents a floating point number to be written in decimal format."""

    def __init__(self, token):
        """
        :param token:  a string.  the text of the number token
        """
        self.text = token
        self.value = float(token)
        eloc = token.find("e")
        if eloc < 0:
            eloc = token.find("E")
        dot_loc = token.find('.')
        if dot_loc < 0:
            self.precision = 0
        else:
            self.precision = eloc - dot_loc - 1

    def __str__(self):
        return f"%.{self.precision}e" % self.value

    def __repr__(self):
        return f'MDLExp("{self}")'

    def __eq__(self, other):
        return self.value == other.value


class MDLString(object):
    """
    Represents an mdl file string which can be several actual strings concatenated together.
    This class preserves the concatenation
    """
    def __init__(self, s=None):
        self.value = s or ""

    def __add__(self, other):
        if isinstance(other, MDLString):
            r = MDLString(self.value + other.value)
            return r
        elif isinstance(other, str):
            r = MDLString(self.value + other)
            return r
        raise TypeError("Expected MDLString but got " + str(type(other)))

    def indented_print(self, indent, start_col):
        first_line_end = 119
        second_line_end = 120

        s = self.value

        #first line
        max_first_line = first_line_end - start_col - 2 # accounting for two double quotes
        if len(s) <= max_first_line:
            return f'"{s}"'

        backslash_count = 0
        ptr = max_first_line - 1
        while s[ptr] == '\\':
            ptr -= 1
            backslash_count += 1
        if backslash_count % 2 > 0:
            size = max_first_line + 1
        else:
            size = max_first_line
        lines = [ f'"{s[:size]}"' ]
        s = s[size:]

        indent_txt = indent_to_ws(indent)
        max_second_line = second_line_end - len(indent_txt) - 2

        while len(s) > max_second_line:
            backslash_count = 0
            ptr = max_second_line - 1
            while s[ptr] == '\\':
                ptr -= 1
                backslash_count += 1
            if backslash_count % 2 > 0:
                size = max_second_line + 1
            else:
                size = max_second_line
            lines.append( f'"{s[:size]}"' )
            s = s[size:]

        if len(s) > 0:
            lines.append(f'"{s}"')

        return f"\n{indent_txt}".join(lines)

    def __repr__(self):
        maxlen = 10
        if len(self.value) == 0:
            return 'MDLString("")'
        if len(self.value)  <= maxlen:
            return f'MDLString("{self.value}")'
        return f'MDLString("{self.value[:maxlen]}...")'

    def __str__(self):
        return self.value


    def __eq__(self, other):
        return str(self) == str(other)

    def set_string(self, s):
        self.value = s


def print_array_value(a):
    if len(a) > 0 and isinstance(a[0], list):
        # is nested list.
        sublists = [", ".join([str(i) for i in b]) for b in a]
        return "[" + "; ".join(sublists) + "]"
    else:
        return '[' + ", ".join([str(i) for i in a]) + ']'


def print_simple_value(val):

    if isinstance(val, list):
        return print_array_value(val)
    return str(val)


def print_value(val, indent, line_length, line_length_chars):
    """
    create a string representation of an object value as it ought to look in an mdl file.

    :param val: the value to print
    :param indent: How many columns to the right this object is indented
    :param line_length: line length so far in columns
    :param line_length_chars: line length so far in chars (needed for string printing)
    :return: returns the string containing value as txt suitable for mdl file
    """
    if isinstance(val, MDLObjectList):
        return " {\n" + val.indent_print(indent+2) + indent_to_ws(indent) + "}"

    # try to start on column 25, adjusted for indent
    txt = indent_to_ws(24+indent, line_length)

    if isinstance(val, MDLString):
        txt += val.indented_print(indent, line_length_chars + len(txt))
    else:
        txt += print_simple_value(val)
    return txt


class MDLObject(object):
    """
    Represents a single MDL object
    """
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __str__(self):
        return self.print()

    def print(self):
        return self.indent_print(0)

    def indent_print(self, indent):
        txt = indent_to_ws(indent)
        txt += self.name

        txt += print_value(self.value, indent, indent + len(self.name), len(txt))
        txt += "\n"
        return txt

    def __repr__(self):
        return f"MDLObject({self.name}, ...)"

    def __eq__(self, other):
        return self.name == other.name and self.value == other.value

    def find(self, predicate):
        """
        Return a list of objects that cause predicate to return true when passed as an argument.
        The list may include this object, or recursively any subobjects.  If predicate(self) is true,
        then self is always the first object on the returned list.
        :param predicate:
        :return:
        """
        if predicate(self):
            ret = [self]
        else:
            ret = []

        if isinstance(self.value, MDLObjectList):
            ret += self.value.find(predicate)

        return ret

    def mutate(self, proc):
        """
        Call proc on self, then potentially call on any recursive values.  Proce may change self.

        :param proc: Takes an MDLObject.  Returns None.  May change the object.
        :return: None
        """
        proc(self)
        if isinstance(self.value, MDLObjectList):
            self.value.mutate(proc)



class MDLParserException(Exception):
    pass


# lexer rules

tokens = """WORD STRING FLOAT EXP INT O_CURLY C_CURLY O_SQUARE C_SQUARE COMMA SEMI"""
tokens = tokens.split()

t_WORD      = r'[$a-zA-Z_][.a-zA-Z0-9_]*'
t_STRING    = r'"([^"\\]|\\.)*"'
t_FLOAT     = r'[-+]?[0-9]+\.[0-9]+'
t_EXP       = r'[-+]?[0-9]+(\.[0-9]+)?[eE][-+]?[0-9]+'
t_INT       = r'[-+]?[0-9]+'
t_O_CURLY   = r'{'
t_C_CURLY   = r'}'
t_O_SQUARE  = r'\['
t_C_SQUARE  = r'\]'
t_COMMA     = r','
t_SEMI      = r';'

t_ignore = " \t"


def t_error(t):
    msg = f"Illegal character at {t.lineno}:{t.lexpos - t.lexer.line_start} '{t.value[0]}'"
    if t.lexer.raise_exception:
        raise MDLParserException(msg)
    else:
        sys.stderr.write(msg + "\n")

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    t.lexer.line_start = t.lexpos + len(t.value) -1

lexer = lex.lex()
lexer.raise_exception = False
lexer.line_start=-1

# parser rules

def prepend_to_array(x, a):
    """prepend x onto a, then return the result.  does not affect a.

    works with nested arrays, so that 1 prepended to [[2,3],[4,5,6]] is prepended as [[1,2,3],[4,5,6]]
    """
    if len(a) == 0:
        return [x]

    if isinstance(a[0], list):
        na = [ prepend_to_array(x, a[0]) ] + a[1:]
        return na

    return [x] + a


def p_objectlist_object(t):
    'objectlist : object objectlist'
    t[0] = MDLObjectList([t[1]] + t[2])

def p_objectlist_end(t):
    'objectlist : '
    t[0] = MDLObjectList([])

def p_object_value(t):
    'object : WORD value'
    t[0] = MDLObject(t[1], t[2])

def p_value_word(t):
    'value : WORD'
    t[0] = Word(t[1])


def p_value_number(t):
    'value : number'
    t[0] = t[1]

def p_value_string(t):
    'value : nonemptystringlist'
    t[0] = t[1]


def p_value_array(t):
    'value : O_SQUARE array C_SQUARE'
    t[0] = t[2]


def p_value_compound(t):
    'value : O_CURLY objectlist C_CURLY'
    t[0] = t[2]

def p_number_int(t):
    'number : INT'
    t[0] = MDLInt(t[1])

def p_number_float(t):
    'number : FLOAT'
    t[0] = MDLFloat(t[1])

def p_number_exp(t):
    'number : EXP'
    t[0] = MDLExp(t[1])

def p_nonemptystringlist_string(t):
    'nonemptystringlist : STRING stringlist'
    t[0] = MDLString(t[1][1:-1]) + t[2]

def p_stringlist_string(t):
    'stringlist : STRING stringlist'
    t[0] = MDLString(t[1][1:-1]) + t[2]

def p_stringlist_empty(t):
    'stringlist : '
    t[0] = MDLString()

def p_array_number(t):
    'array : number arraytail'
    t[0] = prepend_to_array( t[1], t[2])

def p_array_empty(t):
    'array : '
    t[0] = []


def p_arraytail_number(t):
    'arraytail : COMMA number arraytail'
    t[0] = prepend_to_array(t[2], t[3])


def p_arraytail_newrow(t):
    'arraytail : SEMI number arraytail'
    #check if already nested list
    n = t[2]
    if len(t[3]) == 0:
        t[0] = [[], [ n ]]
    elif isinstance(t[3][0], list):
        t[0] = [[]] + prepend_to_array(n, t[3])
    else:
        t[0] = [[], prepend_to_array(n, t[3])]

def p_arraytail_end(t):
    'arraytail : '
    t[0] = []




def p_error(t):
    errpos = t.lexpos - t.lexer.line_start
    msg = f'syntax error at line {t.lexer.lineno} col {errpos}: {t.value}'
    if t.lexer.raise_exception:
        raise MDLParserException(msg)
    else:
        sys.stderr.write(msg + "\n")


parser = yacc.yacc()


def parse_mdl_string(mdl_contents):
    """
    Return a parse tree of the contents of an mdl file

    :param mdl_contents: A string.  Contents of an mdl file to parse.
    :return: An MDL_Object instance that is the root object of the file.
    """
    global parser, lexer
    try:
        root_obj = parser.parse(mdl_contents)
    finally:
        lexer.lineno = 1
    return root_obj


def parse_mdl_file(mdl_filepath):
    """
    Return a parse tree of the contents of an mdl file

    :param mdl_filepath: A string.  Path to an MDL file.
    :return: An MDL_Object instance that is the root object of the file.
    """
    with open(mdl_filepath) as f:
        mdl_contents = f.read()
        return parse_mdl_string(mdl_contents)


def raise_exception_on_error(raise_exception):
    """
    Set to true if parser should raise an exception on an error.  Otherwise, error is printed to std out.

    :param raise_exception:
    :return:
    """
    global lexer
    lexer.raise_exception = raise_exception


def main():
    """
    If run from command line
    :return:
    """
    if len(sys.argv) != 2:
        print("pass mdl file names to parse")
        sys.exit(1)

    root_obj = parse_mdl_file(sys.argv[1])
    sys.stdout.write(str(root_obj))

if __name__ == "__main__":
    main()
