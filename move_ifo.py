#!/bin/env python3
import mdl_parser
from update4_2_0 import is_cdsParam_block, get_params_from_block, set_params_to_block
import argparse
import os.path
import os
import sys
import re


def new_ifo_path(oldifo, newifo, oldpath):
    """get a path to an mdl file that makes sense for the newifo, including changing directory"""
    oldpath, fname = os.path.split(oldpath)
    head = [newifo + fname[2:]]
    while len(oldpath) > 1:
        oldpath, h = os.path.split(oldpath)
        if h == oldifo:
            head = [newifo] + head
            break
        else:
            head = [h] + head
    return os.path.join(oldpath, *head)


def create_move_ifo(newifo):
    def move_ifo(model, path):
        oldifo = model[:2]

        sys.stdout.write(f"changing {path} from {oldifo} to {newifo}.")
        sys.stdout.flush()
        tree = mdl_parser.parse_mdl_file(path)
        sys.stdout.write(".")
        sys.stdout.flush()

        leader_re = re.compile(oldifo.upper() + ':')
        substr = newifo.upper() + ":"

        def change_ifo(obj):
            if obj.name in ["Path", "Name","DstBlock","SrcBlock",
                            'model_', 'Cell'] and isinstance(obj.value, mdl_parser.MDLString):
                if obj.value.value[:2] == oldifo:
                    obj.value.value = newifo + obj.value.value[2:]
                if obj.value.value[:2] == oldifo.upper():
                    obj.value.value = newifo.upper() + obj.value.value[2:]
                obj.value.value = leader_re.sub(substr, obj.value.value)
            elif is_cdsParam_block(obj):
                params = get_params_from_block(obj)
                if 'ifo' in params:
                    params['ifo'] = newifo.upper()
                if 'site' in params:
                    params['site'] = newifo.upper()
                if 'host' in params:
                    params['host'] = newifo + params['host'][2:]
                set_params_to_block(obj, params)

        tree.mutate(change_ifo)
        sys.stdout.write(".")
        sys.stdout.flush()

        new_path = new_ifo_path(oldifo, newifo, path)

        newdir = os.path.dirname(new_path)
        if len(newdir) > 0:
            os.makedirs(newdir, exist_ok=True)

        sys.stdout.write(f"writing to {new_path}...")
        sys.stdout.flush()
        try:
            with open(new_path, "wt") as f:
                f.write(str(tree))
        except IOError as e:
            sys.stderr.write(str(e))
            sys.stdout.write("FAILED\n")
        else:
            sys.stdout.write("done\n")

    return move_ifo


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Copy a model from one ifo to another.")
    parser.add_argument('new_ifo')
    from walk_models import create_main

    main, args = create_main(parser)
    proc = create_move_ifo(args.new_ifo)
    main(proc)



