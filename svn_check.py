#!/bin/env python3
# check if mdl file has been commited to SVN, or commit to svn if not

import sys
import subprocess


def svn_check(repo_path):
    """
    Return True if directory or file pointed to by repo_path is clean.
    :param repo_path: A string pointing to a svn working directory or file in an svn working directory.
    :return: True if repo_path is clean.
    """

    output = subprocess.check_output(['svn', 'status', repo_path])

    return len(output) == 0


def svn_check_model(model, mdl_path):
    if svn_check(mdl_path):
        sys.stdout.write(f". {model} is committed in svn\n")
    else:
        sys.stdout.write(f"m {model} is locally modified\n")


def svn_commit_model(model, mdl_path):
    if svn_check(mdl_path):
        sys.stdout.write(f"{model} is committed in svn\n")
    else:
        sys.stdout.write(f"{model} is locally modified. Committing...")
        sys.stdout.flush()
        try:
            subprocess.run(['svn', 'commit', mdl_path, '-m "committed by svn_check.py script"'])
            sys.stdout.write("committed\n")
        except subprocess.CalledProcessError as e:
            sys.stderr.write(f"{mdl_path} could not be committed: {e}")


if __name__ == "__main__":
    from walk_models import create_main

    main, args = create_main(description="Check that model files are committed to SVN")
    proc = svn_check_model
    main(proc)
