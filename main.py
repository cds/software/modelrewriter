#!env python3

# Modify model blocks in bulk

def read_model_block(model_path):
    """
    Given a path to a model file, return the definition block for the model.

    :param model_path: A string giving the path to the model (.mdl) file.
    :return: A
    """


class ModelFile(object):
    """
    Info stored about a model file.
    """
    current_block = None
    proposed_block = None
    model_path = None
    model_name = None

    def __init__(self, model_name, model_path):
        self.model_name = model_name
        self.model_path = model_path

        self.read_block()

    def read_block(self):
        """
        Re
        :return:
        """






def modify_all_models(noop=False):
    """
    Modify all models found in rtsystab

    :param noop:  When true, don't do anything but print out changes
    :return: None
    """
    # create modification package

    # gather list of models to modify

    # hunt through userapps directory and create new block hashes

    # print summary of changes about to be made



