#!/bin/env python3
# handle model updates as part of 4.2.0 upgrade
import mdl_parser
import shutil
import sys
import os.path as path


def is_cdsParam_block(obj):
    """
    Check if an MDL object is a CDS parameter block
    :param obj: An MDLObject
    :return: True if the object is a CDS paramter block
    """
    if obj.name == "Block" and isinstance(obj.value, mdl_parser.MDLObjectList):
        for x in obj.value:
            if x.name == "Tag" and x.value == "cdsParameters":
                return True
    return False

def is_cdsParam_externalreference(obj):
    """
    Check if an MDL objec is a CDS parameter external reference
    :param obj:
    :return: True if it is sucha reference
    """
    if obj.name == "ExternalFileReference" and isinstance(obj.value, mdl_parser.MDLObjectList):
        for x in obj.value:
            if x.name == 'Reference' and x.value == "cdsParameters/Subsystem":
                return True
    return False


class ParamBlockException(Exception):
    pass


def create_param_string(params, sep):
    """
    Create a param string suitable for an MDL file.
    :param params: A dictionary of the parameters and their values
    :param sep: A separator betwen values.
    :return: A string suitable for an MDL file.
    """

    order = "ifo rate dcuid host iop_model pciRfm dolphin_time_xmit dolphin_time_rcvr specific_cpu rfm_delay".split()
    unused = list(params.keys())

    txts = []
    for p in order:
        if p in params:
            txts.append(f"{p}={params[p]}")
            unused.remove(p)

    for p in unused:
        txts.append(f"{p}={params[p]}")

    return sep.join(txts)


def get_param_block(model, ptree):
    """

    :param model: model name
    :param ptree:  Root object of an mdl file parse tree
    :return:
    """
    param_blocks = ptree.find(is_cdsParam_block)
    if len(param_blocks) == 0:
        raise ParamBlockException(f"Could not find param block in {model}")
    elif len(param_blocks) > 1:
        raise ParamBlockException(f"Found more than 1 param block in {model}")
    return param_blocks[0]


def get_params_from_block(block):
    """
    From a param Block object, get a dictionary of name: value pairs for the parameters
    :param block: an MDLObject representing the param block
    :return: A dictionary of the parameters
    """
    for obj in block.value:
        if obj.name == "Name":
            return {param.split('=', 1)[0] : param.split('=', 1)[1]
                    for param in str(obj.value).split(r'\n')
                    if param.find('=') >= 0}

    raise ParamBlockException("Couldn't find 'Name' object in cdsParameters block.")


def set_params_to_block(block, params):
    for obj in block.value:
        if obj.name == "Name":
            param_txt = create_param_string(params, '\\n')
            obj.value.set_string(param_txt)
            return

    raise ParamBlockException("Couldn't find 'Name' object in cdsParameters block.")


def set_params_to_external_reference(model, ptree, params):
    paramtxt = create_param_string(params, " ")
    txt = f"{model}/{paramtxt}"

    refs = ptree.find(is_cdsParam_externalreference)

    for ref in refs:
        for obj in ref.value:
            if obj.name == 'Path':
                obj.value.set_string(txt)


# upgrades
def change_name(params, old_name, new_name):
    """
    Change a param name from old to new
    :returns: number of items changed
    """
    if old_name in params:
        params[new_name] = params[old_name]
        del params[old_name]
        return 1
    return 0


def delete_param(params, param_name):
    """

    :param params:
    :param param_name:
    :return: number of items changed
    """
    if param_name in params:
        del params[param_name]
        return 1
    return 0


def upgrade_iop_model(params):
    num_changed = 0
    num_changed += change_name(params, 'site', 'ifo')
    num_changed += delete_param(params, 'shmem_daq')
    num_changed += change_name(params, 'adcMaster', 'iop_model')
    num_changed += delete_param(params, 'rfm_dma')

    if 'virtualIOP' in params and int(params['virtualIOP']) == 2:
        del params['virtualIOP']
        params['dolphin_time_rcvr'] = 1
        num_changed += 1
    return num_changed


def upgrade_user_model(params):
    num_changed = 0
    num_changed += change_name(params, 'site', 'ifo')
    num_changed += delete_param(params, 'shmem_daq')
    num_changed += delete_param(params, 'adcSlave')
    num_changed += delete_param(params, 'biquad')
    num_changed += delete_param(params, 'rfm_dma')
    num_changed += delete_param(params, 'accum_overflow')
    return num_changed


def upgrade_params(params):
    """
    Return a params dictionary changed to match those expected by 4_2_0
    :param params:
    :return: number of items changed
    """
    if (('adcMaster' in params and int(params['adcMaster']) > 0)
            or ('iop_model' in params and int(params['iop_model']) > 0)):
        return upgrade_iop_model(params)
    else:
        return upgrade_user_model(params)


def check_rate(model, params):
    """
    sus iop models need ipc rate set
    :param model:
    :param params:
    :return: number of items changed
    """
    if (model.find("iopsus") >= 0
            and model.find("iopsusaux") < 0
            and not ('ipc_rate' in params and int(params['ipc_rate']) == 2048)):
        sys.stdout.flush()
        params['ipc_rate'] = 2048
        return 1
    return 0


def upgrade_model(model, mdl_path):
    sys.stdout.write(f'upgrading params in {model}.')
    sys.stdout.flush()
    ptree = mdl_parser.parse_mdl_file(mdl_path)
    sys.stdout.write('.')
    sys.stdout.flush()
    block = get_param_block(model, ptree)
    new_params = get_params_from_block(block)
    sys.stdout.write('.')
    sys.stdout.flush()
    num_changed = upgrade_params(new_params)
    sys.stdout.write('.')
    sys.stdout.flush()
    num_changed += check_rate(model, new_params)
    sys.stdout.write('.')
    sys.stdout.flush()

    if num_changed > 0:
        set_params_to_block(block, new_params)
        set_params_to_external_reference(model, ptree, new_params)

        # backup
        try:
            backup_base_name = mdl_path + ".rcg4.0"
            backup_name = backup_base_name
            backup_number = 1
            while path.exists(backup_name):
                backup_number += 1
                backup_name = backup_base_name + "_" + str(backup_number)
            shutil.copyfile(mdl_path, backup_name)
            sys.stdout.write('.')
            sys.stdout.flush()
            with open(mdl_path, "wt") as f:
                f.write(str(ptree))
                sys.stdout.write(".done\n")
        except Exception as e:
            sys.stdout.write(".FAILED\n")
            sys.stderr.write(str(e))
    else:
        sys.stdout.write(f'model is up to date. File unchanged.\n')


if __name__ == "__main__":
    from walk_models import create_main

    main, args = create_main(description="Update models with parameters expected in RCG 4.2")
    main(upgrade_model)
