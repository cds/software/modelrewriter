#!/bin/env python3
# create a text file in /opt/rtcds/userapps/mdlfiles.txt that is a list of the mdl file paths for all models.

def add_to_list(model, mdl_path):
    """
    Add mdl_path to mdl_file_path file

    :param model:
    :param mdl_path:
    :return:
    """
    global mdl_file_path
    with open(mdl_file_path, "at") as f:
        f.write(f"{mdl_path}\n")
        print(f"added {mdl_path}")

if __name__ == "__main__":
    from walk_models import create_main
    from argparse import ArgumentParser

    global mdl_file_path

    parser = ArgumentParser(description="create a text file that is a list of mdl paths for all given models.")

    parser.add_argument("-o", "--outfile", help="path to output file", default="/opt/rtcds/userapps/mdlfiles.txt")

    main, args = create_main(parser=parser)

    mdl_file_path = args.outfile
    print(f"filling {mdl_file_path} with mdl paths")
    with open(mdl_file_path, "wt"):
        pass
    main(add_to_list)