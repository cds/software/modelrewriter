#!/bin/env python3
# checks on the model file
import mdl_parser
import sys
import os.path as path
import subprocess

mdl_parser.raise_exception_on_error(True)


# parser checks
def check_parse(model, mdl_path):
    """
    Check that an mdl file is parsed by the parser without errors

    Outputs first error message to stderr if there is a failure
    :param model: model name.
    :param mdl_path: Path to mdl file.
    :return: True if passes.
    """

    sys.stdout.write(f"Parsing mdl file for {model}...")
    try:
        mdl_parser.parse_mdl_file(mdl_path)
    except mdl_parser.MDLParserException as e:
        sys.stderr.write(f"Error parsing {mdl_path}: {str(e)}\n")
        sys.stdout.write("failed\n")
    else:
        sys.stdout.write("done\n")


def check_diff(model, mdl_path):
    """
    Check difference between parsed output and mdl file.

    :param model:
    :param mdl_path:
    :return:
    """
    target_path = "/home/controls/modelrewriter/diffs"

    sys.stdout.write(f"Comparing mdl file to parser output for {model}...")
    try:
        parser_out = str(mdl_parser.parse_mdl_file(mdl_path))
        with open(mdl_path) as f:
            mdl_contents = f.read()

        if mdl_contents == parser_out:
            sys.stdout.write(f"matched\n")
        else:
            diff_name = path.join(target_path, model + ".dif")
            tmp_name = f"/tmp/model_out.mdl"
            with open(tmp_name, "wt") as f:
                f.write(parser_out)
            with open(diff_name, "wt") as f:
                subprocess.run(["diff", tmp_name, mdl_path], stdout=f, text=True)
            sys.stdout.write(f"diff written to {diff_name}\n")

    except mdl_parser.MDLParserException as e:
        sys.stderr.write(f"Error parsing {mdl_path}: {str(e)}\n")
        sys.stdout.write("failed\n")
    else:
        sys.stdout.write("done\n")


def check_reparse(model, mdl_path):
    sys.stdout.write(f"Checking that parser output parses identically to mdl for {model}.")
    sys.stdout.flush()
    try:
        parser_tree = mdl_parser.parse_mdl_file(mdl_path)
        sys.stdout.write(".")
        sys.stdout.flush()
        second_pass = mdl_parser.parse_mdl_string(str(parser_tree))
        if parser_tree == second_pass:
            sys.stdout.write(".match\n")
        else:
            sys.stdout.write("FAIL\n")
    except mdl_parser.MDLParserException as e:
        sys.stderr.write(f"Error parsing {mdl_path}: {str(e)}\n")
        sys.stdout.write("..failed\n")


def runall():
    from walk_models import walk_models
    walk_models(check_reparse)

def runiop():
    from walk_models import walk_models, get_iop_list
    walk_models(check_reparse, models=get_iop_list())

def run():
    fname = sys.argv[1]
    check_reparse("test", fname)


if __name__ == "__main__":
    from walk_models import create_main
    main, args = create_main(description="Check that models parse and that generated mdl files parse identically")
    proc = check_reparse
    main(proc)
